# PlantsAndCo
- pip install requests

- django-admin startapp plantsapi




# DOC

- https://pypi.org/project/requests/
- http://zetcode.com/django/httpresponse/



# API Response model :

<script>
    "data":[
        {
        "id":678281,
        "common_name":"Evergreen oak",
        "slug":"quercus-rotundifolia",
        "scientific_name":"Quercus rotundifolia",
        "year":1785
        "bibliography":"Encycl. 1: 723 (1785)",
        "author":"Lam.",
        "status":"accepted",
        "rank":"species",
        "family_common_name":"Beech family",
        "genus_id":5778,
        "image_url":"https://bs.floristic.org/image/o/1a03948baf0300da25558c2448f086d39b41ca30",
        "synonyms":["Quercus lyauteyi","Quercus rotundifolia f. crassicupulata","Quercus ballota","Quercus ilex f. brevicupulata","Quercus calycina","Quercus rotundifolia f. dolichocalyx","Quercus rotundifolia f. pilosella","Quercus rotundifolia f. macrocarpa","Quercus rotundifolia f. calycina","Quercus ilex f. macrocarpa","Quercus ilex subsp. ballota","Quercus rotundifolia var. pilosella","Quercus rotundifolia var. brevicupulata","Quercus rotundifolia subsp. maghrebiana","Quercus rotundifolia f. brevicupulata","Quercus rotundifolia var. macrocarpa"],
        "genus":"Quercus",
        "family":"Fagaceae",
        "links":{
            "self":"/api/v1/species/quercus-rotundifolia",
            "plant":"/api/v1/plants/quercus-rotundifolia",
            "genus":"/api/v1/genus/quercus"}
        }]
    "links": {
        "self": "/api/v1/plants"
        "first": "/api/v1/plants?page=1"
        "next": "/api/v1/plants?page=2"
        "last": "/api/v1/plants?page=18879"
    }
</script>



# Construction

--> Récupération des data API, dans les views de l'application.

--> Traitements des donnees dans les function.

--> Renvoi de variables d'affichages dans les vues.


#### Page d'accueill :
    - Présentation
    - affichage quelques plantes
    - menu 
        - recherche
        - toutes les plantes par Pays :
            - Guyane
            - Australie
            - Bresil
            - Russie
            ...
    
#### Page de Recherche :
    - liste de plantes
    - pagination
    - input de recherche, criteres en fonction de l'api

#### Page Single :
    - Vue pour une plante

#### Page Single Plantes du Pays :
    - Vue des plantes du Pays selectionné dans le menu.





TODO:
- mettre les pages html en model (includes)
- gerer fonction pagination (se servir des links dispos dans le resultat de la request API)
- gerer la page SinglePlant
- gerer fonction recherche