from django.shortcuts import render
from django.views.generic import TemplateView
from .services import get_all_plants, get_plant


# All plants vue
class GetPlants(TemplateView):

    template_name = 'index.html'

    def get_page(self):
        return reverse('plant_detail', kwargs={'page': self.page})   

    def get_context_data(self, *args, **kwargs):
        print(self, args, kwargs)
        context = {
            'plants' : get_all_plants(),
        }
        return context


# One plant vue
class GetSinglePlant(TemplateView):

    template_name = 'plant.html'

    def get_absolute_url(self):
        return reverse('plant_detail', kwargs={'slug': self.slug})    

    def get_context_data(self, *args, **kwargs):
        context = {
            'plant' : get_plant(kwargs['slug']),
        }
        return context