import os
import requests


# Récupération de toute les plantes
def get_all_plants(page="/api/v1/plants?page=1"):

    # Mise en variable de l'url a questionner
    url = 'https://trefle.io' + page + '&token=' + os.getenv('DO_ACCESS_TOKEN')

    # Questionement a l'API
    response = requests.get(url)

    # Récupération des plantes au format json
    plants = response.json()

    # Renvoi des données collectée (liste de plantes)
    return plants




# Récupération d'une plante
def get_plant(slug):
    
    url = 'https://trefle.io/api/v1/plants/' + slug + '?token=' + os.getenv('DO_ACCESS_TOKEN')
    response = requests.get(url)
    plants = response.json()

    return plants['data']
